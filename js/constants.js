if (typeof chrome.runtime.id !== "undefined") {
    window.MINTOIL_CONSTANTS = {
        application: {
            name: chrome.i18n.getMessage("extName")
        },
        logger: {
            style: (function () {
                let _defaultStyle = [
                    `border-right: 1px solid #266B36`,
                    `padding-left: 16px`,
                    `padding-right:5px`,
                    `font-size: 12px`,
                    `line-height: 16px`,
                    `vertical-align: middle`,
                    `color:._266B36`,
                    `background: url(chrome-extension://${chrome.runtime.id}/images/icons/16.png) top left no-repeat`,
                    `background-size: 13px 13px`
                ];
                return _defaultStyle.join(';');
            })()
        }
    };
}