#!/usr/bin/env node
const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
const config = require('dotenv').config();
let cwd = __dirname + '/../';
cwd = cwd.replace(/\\/gi, '/');

const manifest = {
    manifest_version: 2,
    name: 'MintOil™ for use with Intuit® Mint™',
    short_name: '__MSG_extName__',
    version: '0.1.11',
    version_name: '0.1-alpha',
    update_url: 'https://clients2.google.com/service/update2/crx',
    default_locale: 'en',
    description: '__MSG_extDesc__',
    minimum_chrome_version: '72',
    icons: {
        16: 'images/icons/16.png',
        48: 'images/icons/48.png',
        128: 'images/icons/128-store.png'
    },
    browser_action: {
        default_icon: {
            16: 'images/icons/16.png',
            24: 'images/icons/24.png',
            32: 'images/icons/32.png',
            48: 'images/icons/48.png',
            128: 'images/icons/128.png'
        },
        default_title: 'MintOil'
    },
    author: 'CaelusMinds',
    background: {
        persistent: true,
        scripts: [
            'js/constants.js',
            'js/lib/logger.js',
            'models/mintoil.js',
            'src/bg/background.js'
        ]
    },
    content_scripts: [
        {
            matches: [
                '*://mint.intuit.com/*'
            ],
            js: [
                'js/lib/google.api.js',
                'js/constants.js',
                'js/lib/logger.js',
                'src/inject/mint-injection.js'
            ],
            css: [
                'src/inject/mint-injection.css'
            ],
            all_frames: false,
            run_at: 'document_start'
        }
    ],
    content_security_policy: `default-src 'self' https://mint.images.com https://www.googleapis.com ;script-src 'self' https://images.mint.com; connect-src 'self' https://www.googleapis.com https://images.mint.com data:; style-src 'unsafe-inline'; img-src * 'self' data: https: chrome-extension-resource:;`,
    web_accessible_resources: [
        'js/lib/*.js',
        'js/constants.js',
        'models/mintoil.js',
        'images/*.png',
        'images/*.svg',
        'images/logos/*.png',
        'images/logos/*.svg',
        'images/icons/*.png',
        'images/icons/*.svg',
        'css/**/*.css',
        'css/**/*.png',
        'css/fonts/fontawesome-webfont.eot',
        'css/fonts/fontawesome-webfont.svg',
        'css/fonts/fontawesome-webfont.ttf',
        'css/fonts/fontawesome-webfont.woff',
        'css/fonts/fontawesome-webfont.woff2',
        'src/inject/mint-extended.js',
        'src/inject/*.js',
        'src/inject/*.css',
        'src/inject/custom-page/custom-page.*',
        'src/inject/monthly-calc/monthly-calc.*',
        'src/inject/date-searches/date-searches.*'
    ],
    externally_connectable: {
        matches: [
            '*://mint.intuit.com/*',
            '*://images.mint.com/*'
        ]
    },
    homepage_url: 'https://mintoil.caelusminds.com',
    incognito: 'split',
    offline_enabled: false,
    oauth2: {
        client_id: process.env.CLIENT_ID,
        scopes: [
            'profile',
            'email',
            'openid',
            'https://www.googleapis.com/auth/chromewebstore.readonly'
        ]
    },
    permissions: [
        'webNavigation',
        'tabs',
        'contextMenus',
        'declarativeContent',
        'identity',
        'identity.email',
        'notifications',
        'storage',
        'unlimitedStorage',
        '*://mint.intuit.com/*',
        '*://images.mint.com/*',
        'https://www.googleapis.com/'
    ],
    key: process.env.MANIFEST_KEY
};

fs.writeFile('manifest.json', JSON.stringify(manifest, null, 2), function (err, file) {
    if (err){
        console.error("Error has occurred while preparing the manifest.json file.", err);
    }
    console.log(chalk.green('The manifest.json file was successfully populated.'));
});