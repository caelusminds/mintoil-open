// Let's scope this thing, so no one can play with.
((require) => {
    //#region Constants and method extensions

    const isDebug = localStorage.getItem('env') === 'debug';

    /**
     * @type Intl#NumberFormatOptions
     */
    /* noinspection JSValidateType */
    const FORMAT_OPTIONS = {minimumFractionDigits: 2, maximumFractionDigits: 2};

    /**
     * @type Intl#NumberFormatOptions
     */
    /* noinspection JSValidateTypes */
    const FORMAT_OPTIONS_INPUT = {minimumFractionDigits: 0, maximumFractionDigits: 2};

    /**
     * Abstract function for handling this function's conversion.
     * @function localStringToNumber
     * @returns {number} Cannot exceed 999,999.99 (for this instance)
     */
    const localStringToNumber = function () {
        let retVal = Number(String(this).replace(/[^0-9.-]+/g, ""));
        if (retVal > 999999.99) {
            retVal = 999999.99;
        }
        return retVal;
    };

    /**
     *  Takes a local string and properly converts it to a number, removing any non-numeric chars.
     *  @function String#localStringToNumber
     *  @returns {number} Cannot exceed 999,999.99 (for this instance)
     */
    String.prototype.localStringToNumber = localStringToNumber;

    /**
     *  Takes a local number and properly converts it to a fixed number, removing any non-numeric chars.
     *  @function Number#localStringToNumber
     *  @returns {number} Cannot exceed 999,999.99 (for this instance)
     */
    Number.prototype.localStringToNumber = localStringToNumber;

    const CurrencySettings =
        /**
         * @class CurrencySettings
         * @property {string} [locale] Locale to use. Default is "en-US".
         * @property {Intl.NumberFormatOptions} [options] Format objects. Defaults are set for 2 decimal places.
         */
        function () {
            // noinspection JSUnusedGlobalSymbols
            this.locale = 'en-US';
            // noinspection JSUnusedGlobalSymbols
            this.options = FORMAT_OPTIONS;
        };

    /**
     * An extension of the [String.toLocaleString()]{@link String#toLocaleString} function.
     * Pre-formats the locale with en-US and forces 2 decimal places.
     * @function Number#toCurrencyString
     * @param {CurrencySettings|Intl#NumberFormatOptions|any} [settings]
     * @returns {string} A pre-formatted string with two-decimals.
     */
    Number.prototype.toCurrencyString = function (settings) {
        if ((typeof settings == "undefined") || settings === null) {
            settings = new CurrencySettings();
            settings.locale = "en-US";
            settings.options = FORMAT_OPTIONS;
        }

        if ((typeof settings !== "object")) return this;

        return this.toLocaleString(settings.locale, settings.options || settings);
    };
    String.prototype.toCurrencyString = function (settings) {
        return this.localStringToNumber().toCurrencyString(settings);
    };

    /**
     * An extension to the array object type to allow a collection to become a csv
     * @function Array#toCSV
     * @returns {string} Comma-delimited values.
     */
    Array.prototype.toCSV = function () {
        let objArray = this;
        let currentArray =
            typeof objArray !== "object" ? JSON.parse(objArray) : objArray;
        let str = "";
        let headers = Object.keys(objArray[0]);

        for (let i = 0; i < currentArray.length; i++) {
            let line = "";
            for (let index in currentArray[i]) {
                if (line !== "") line += ",";
                // noinspection JSUnfilteredForInLoop
                let lineValue = currentArray[i][index];
                let lineValueType = typeof lineValue;
                if (lineValueType === "object" || lineValueType === "array") {
                    lineValue = "~complex-object~";
                }
                line += `${lineValue}`;
            }

            str += line + "\r\n";
        }

        str = headers.map(d => `${d}`).toString() + "\r\n" + str;

        return str;
    };

    // noinspection JSUnusedGlobalSymbols
    /**
     * Checks a promise status.
     */
    Promise.prototype.Query = function () {
        // Don't modify any promise that has been already modified.
        if (this.isResolved) return this;

        // Set initial state
        let isPending = true;
        let isRejected = false;
        let isFulfilled = false;

        // Observe the promise, saving the fulfillment in a closure scope.
        let result = this.then(
            function (v) {
                isFulfilled = true;
                isPending = false;
                return v;
            },
            function (e) {
                isRejected = true;
                isPending = false;
                throw e;
            }
        );

        result.isFulfilled = () => isFulfilled;
        result.isPending = () => isPending;
        result.isRejected = () => isRejected;
        return result;
    };
    //#endregion

    // Let us begin the actual work here...
    require(
        //#region REQUIRE :: Module imports
        [
            "jquery",
            "moment",
            "underscore",
            "models/bps/Bill",
            "models/bps/Bills",
            "models/bps/PaymentMethods",
            "models/appservice/BillSuggestions",
            "models/appservice/ProviderSuggestions",
            "models/appservice/Providers",
            "models/bps/Receipts",
            "models/bps/BillsHistory",
            "models/bps/BillerConfigurations",
            "services/data/DynamicProperties",
            "lib/mint/MintPopoverView"
        ].concat([
            // Because WebStorm doesn't like it when I use these paths, lol
            `chrome-extension://{{chrome.runtime.id}}/js/lib/logger${!isDebug ? '.min' : ''}.js`,
            `chrome-extension://{{chrome.runtime.id}}/js/lib/localforage.min.js`,
            `chrome-extension://{{chrome.runtime.id}}/js/lib/FileSaver.min.js`,
            `chrome-extension://{{chrome.runtime.id}}/js/lib/jquery.dataTables.min.js`,
            `chrome-extension://{{chrome.runtime.id}}/js/lib/jquery.tabbable${!isDebug ? '.min' : ''}.js`,
            `chrome-extension://{{chrome.runtime.id}}/js/constants${!isDebug ? '.min' : ''}.js`
        ])
        //#endregion
        ,
        (
            //#region  REQUIRE :: Module definitions
            $,
            moment,
            _,
            billModel,
            billsModel,
            paymentsModel,
            billSuggestions,
            providerSuggestions,
            providers,
            receiptsModel,
            billsHistory,
            billerConfigModel,
            dynamicProperties,
            MintPopoverView,
            Logger,
            localForage
            //#endregion
        ) => {

            //const history = billsHistory.getInstance();
            const receipts = receiptsModel.getInstance();

            // noinspection JSUnusedLocalSymbols
            let currentDatePicker = $("#leftColCal").first().data("datepicker");
            const calendarGrouping = new class {

                get currentMonthGrouping() {
                    return `${this._datePicker.currentYear}-${(this._datePicker.currentMonth + 1)}`;
                }

                get selectedMonthGrouping() {
                    return `${this._datePicker.selectedYear}-${(this._datePicker.selectedMonth + 1)}`;
                }

                get currentMonth() {
                    return this._datePicker.currentMonth + 1;
                }

                get selectedMonth() {
                    return this._datePicker.selectedMonth + 1;
                }

                get DatePicker() {
                    return this._datePicker;
                }

                constructor() {
                    this._datePicker = currentDatePicker;
                }
            }();

            //#region Logger
            /* This is to make sure we can tell MintOil logs apart from standard console logs. */
            // noinspection JSUnusedLocalSymbols
            Logger.useDefaults({
                formatter: function (messages, context) {
                    messages.splice(0, 0, `%c{{MINTOIL_CONSTANTS.application.name}}-Injected`);
                    messages.splice(1, 0, `{{MINTOIL_CONSTANTS.logger.style}}`);
                }
            });

            // We don't really need logging if the environment isn't set for it.
            const logger = Logger.get('Injected');
            if (!isDebug) {
                logger.setLevel(Logger.ERROR);
            } else {
                logger.setLevel(Logger.TRACE);
            }
            //#endregion

            //#region Local Forage (DB)
            // Create instance of our localForage
            const mintOilCalcPageOptions = new class {

                // noinspection JSUnusedGlobalSymbols
                get HideFullyPaid() {
                    let retVal;

                    if (typeof this._hideFullyPaid !== "undefined") {
                        retVal = this._hideFullyPaid;
                    } else {
                        retVal = (async (self) => {
                            return await self.dbInstance.getItem("hide-fully-paid");
                        })(this).catch(err => logger.error(err));
                    }
                    return retVal;
                }

                set HideFullyPaid(value) {
                    this.dbInstance.setItem("hide-fully-paid", value)
                        .then(value => {
                            logger.debug("Options Changed :: HideFullyPaid()", {value});
                            this._hideFullyPaid = value;
                        })
                        .catch(err => logger.error(err));
                }

                get ExportInputColumns() {
                    let retVal;

                    if (typeof this._exportInputColumns !== "undefined") {
                        retVal = this._exportInputColumns;
                    } else {
                        retVal = (async (self) => {
                            return await self.dbInstance.getItem("export-input-columns");
                        })(this).catch(err => logger.error(err));
                    }

                    return retVal;
                }

                set ExportInputColumns(value) {
                    this.dbInstance.setItem("export-input-columns", value)
                        .then(value => {
                            logger.debug("Options Changed :: ExportInputColumn()", {value});
                            this._exportInputColumns = value;
                        })
                        .catch(err => logger.error(err));
                }

                constructor() {
                    this._hideFullyPaid = undefined;
                    this._exportInputColumns = undefined;

                    this.dbInstance =
                        localForage.createInstance({
                            "name": "mintoil",
                            "version": 1.0,
                            "storeName": "options",
                            "description": "Place to store user-set options for MintOil."
                        });
                    this.dbInstance.getItem("hide-fully-paid").then(value => this._hideFullyPaid = value);
                    this.dbInstance.getItem("export-input-columns").then(value => this._exportInputColumns = value);
                }
            }();

            // Create Instance for saved payment calc data
            /**
             * Encapsulates the methods and members of the CalcStorage object.
             * @class CalcStorage
             * @property {LocalForageDriver} dbInstance
             */
            const calcStore = new class {
                /**
                 * Adds or Updates a given fields value in the calculation page,
                 * grouped by the month_group variable.
                 * @param {string}  field           Name of the field to save.
                 * @param {number}  value           Value of the field being saved.
                 * @param {string}  [month_group]   Optional. Month grouping of the save.
                 * @return {Promise<boolean>}
                 */
                async upsert(field, value, month_group) {
                    let groupingRecord,
                        retVal = true;
                    month_group = month_group
                        ? moment(month_group).format("YYYY-MM")
                        : moment(calendarGrouping.selectedMonthGrouping).format("YYYY-MM");
                    groupingRecord = await this.dbInstance.getItem(month_group);

                    if (groupingRecord == null) {
                        groupingRecord = {};
                    }

                    if (groupingRecord.hasOwnProperty(field)) {
                        if (!_.isEqual(groupingRecord[field], value)) {
                            groupingRecord[field] = value;
                        }
                    } else {
                        groupingRecord[field] = value;
                    }

                    await this.dbInstance.setItem(month_group, groupingRecord).catch(err => {
                        logger.error(err);
                        retVal = false;
                    });

                    return retVal;
                }

                /**
                 * Gets the given field's value in the calculation page.
                 * @param {string}  field           Name of the field to save.
                 * @param {string}  [month_group]   Optional. Month grouping of the save.
                 * @return {Promise<null|*>}
                 */
                async get(field, month_group) {
                    let groupingRecord,
                        retVal = null;

                    month_group = month_group
                        ? moment(month_group).format("YYYY-MM")
                        : moment(calendarGrouping.selectedMonthGrouping).format("YYYY-MM");

                    groupingRecord = await this.dbInstance.getItem(month_group);

                    if (groupingRecord == null) {
                        groupingRecord = {};
                    }

                    if (groupingRecord.hasOwnProperty(field)) {
                        retVal = groupingRecord[field];
                    }

                    return retVal;
                }

                async setRange(range, month_group) {
                    month_group = month_group
                        ? moment(month_group).format("YYYY-MM")
                        : moment(calendarGrouping.selectedMonthGrouping).format("YYYY-MM");
                    return await this.dbInstance.setItem(month_group, range);
                }

                constructor() {
                    this.dbInstance =
                        localForage.createInstance({
                            "name": "mintoil",
                            "version": 1.0,
                            "storeName": "calc",
                            "description": "Place to store user-set calculations by month"
                        });
                }
            }();
            //#endregion

            //#region Runtime Functions
            /**
             * Calculate the ideal payment total
             * @function
             * @returns {number}
             */
            function calcIdealPayment() {
                let totalPayWish = 0.00;
                $("span.calc_custom_currency").each(function () {
                    totalPayWish += $(this).html().localStringToNumber();
                });
                return totalPayWish;
            }

            //#region Option check boxes
            /**
             * Initializes the check-boxes we use for the options.
             * @function
             * @returns void
             */
            let initCheckbox = () => {
                let $checkOptionsShowFullyPaid = $("#options-hide-fully-paid");
                let $checkExportInputColCalcPageOptions = $("#options-export-input-columns");

                /*if ($checkOptionsShowFullyPaid.length === 0){
                    logger.debug("INIT CHECKBOX TIMEOUT", $checkOptionsShowFullyPaid);
                    setTimeout(initCheckbox, 350);
                    return;
                }*/

                let runHide = (hide) => {
                    if (hide) {
                        $(".paid-in-full").hide();
                    } else {
                        $(".paid-in-full").show();
                    }
                };

                $checkOptionsShowFullyPaid.on("change", function () {
                    let hide = $(this).is(":checked");
                    mintOilCalcPageOptions.HideFullyPaid = hide;
                    runHide(hide);
                });
                Promise.resolve(mintOilCalcPageOptions.HideFullyPaid).then(value => {
                    $checkOptionsShowFullyPaid.prop("checked", value);
                    runHide(value);
                });


                $checkExportInputColCalcPageOptions.on("change", function () {
                    mintOilCalcPageOptions.ExportInputColumns = $(this).is(":checked");
                });
                Promise.resolve(mintOilCalcPageOptions.ExportInputColumns).then(value => {
                    $checkExportInputColCalcPageOptions.prop("checked", value);
                })
            };

            //#endregion

            /**
             * Gets the DataTable data.
             * @async
             * @function
             * @param {object} data Data to send to the server.
             * @param {function} callback The callback function that must be executed when the required data has
             * been obtained. That data should be passed into the callback as the only parameter.
             * @param {DataTables#Settings} settings DataTables settings object.
             */
            async function getDataTableData(data, callback, settings) {
                // noinspection JSUnusedLocalSymbols
                // noinspection JSUnusedAssignment
                let bills = [],
                    sortedBills = [],
                    messages = {show: false},
                    billsInstance = billsModel.getInstance(),
                    providersInstance = providers.getInstance();

                providersInstance.getAccountsMapForOverviewModule()
                    .then(accountsData => {
                        let bankAccountsMap = accountsData.accountsMap && accountsData.accountsMap.bank;
                        MintOil.TotalLiquidity = (bankAccountsMap && bankAccountsMap.metaData && bankAccountsMap.metaData.value) || 0;
                    });

                let billsByProvider = providersInstance.getAllProviderAccounts({filterClosedAccounts: true})
                    .filter(account => account.get("accountId_BPS"))
                    .map(account => {
                        let retVal = billsInstance.get({id: account.get("accountId_BPS")});
                        let billDetails = retVal.get("billDetailsList") || [];
                        if (billDetails.length > 0) {
                            retVal.details = billDetails[0];
                        } else {
                            retVal.details = null;
                        }
                        retVal.model = retVal.get("model");
                        retVal.isNow = true;
                        return retVal;
                    });

                if (calendarGrouping.currentMonthGrouping !== calendarGrouping.selectedMonthGrouping) {
                    let futureBills = [],
                        monthDiff = calendarGrouping.selectedMonth - calendarGrouping.currentMonth;

                    _.each(billsByProvider, (bill) => {
                        let biller = bill.getBiller();
                        let frequency = biller.get("frequency") || "MONTH";
                        let repeatInterval = biller.get("repeatInterval") || 1;
                        let copy = $.extend(true, {}, bill);
                        let dueDate = bill.dueDate || bill.get('dueDate');
                        let originalBillMonth = moment(dueDate).month() + 1;

                        if (originalBillMonth <= calendarGrouping.selectedMonth) {
                            monthDiff = calendarGrouping.selectedMonth - originalBillMonth;
                        }

                        if (frequency === 'WEEK') {
                            copy.dueDate = moment(dueDate).add((monthDiff) * repeatInterval, 'weeks').format('YYYY-MM-DD');
                        } else if (frequency === 'MONTH') {
                            copy.dueDate = moment(dueDate).add((monthDiff) * repeatInterval, 'months').format('YYYY-MM-DD');
                        }
                        let billDetails = copy.get('billDetailsList') ? copy.get('billDetailsList')[0] : {availableBalanceAmount: copy.getListAmount()};

                        copy.billStatus = 'NOT_PAID';
                        copy.dueAmount = copy.get('aggregationDueAmount') || billDetails.availableBalanceAmount || copy.aggregationDueAmount;
                        copy.minDueAmount = copy.get('aggregationMinimumPaymentDue') || copy.dueAmount;
                        copy.amountPaid = 0.00;
                        copy.isFuture = monthDiff > 0;
                        copy.isPast = monthDiff < 0;
                        copy.isNow = false;

                        futureBills.push(copy);
                    });

                    billsByProvider = futureBills;
                }

                logger.debug("All Providers", billsByProvider);

                sortedBills = billsInstance.getSortedByDate(billsByProvider);

                bills = sortedBills.map((_bill) => {
                    // noinspection JSUnusedAssignment
                    let payments,
                        amountDue = 0.00,
                        amountDueMin = 0.00,
                        amountPaid = 0.00,
                        notes = ``;

                    // Amount Due, should always show...
                    if (!isNaN(_bill.getAmountDue())) {
                        amountDue = _bill.getAmountDue().toCurrencyString();
                    } else {
                        if (_bill.isCreditCardBill()) {
                            amountDue = billsInstance.getTotalFromReceipts(_bill);
                        } else {
                            amountDue = _bill.getListAmount();
                        }

                        if (amountDue === 0.00) {
                            let details = _bill.get("billDetailsList");
                            if (details.length > 0 && (typeof details[0]["availableBalanceAmount"] !== "undefined")) {
                                amountDue = details[0]["availableBalanceAmount"].toCurrencyString();
                            }
                        }

                        if (amountDue === 0.00) {
                            amountDue = "--";
                        }
                    }

                    payments = ((_bill, receipts) => {
                        let retVal = [];

                        try {
                            retVal = receipts.filter(f =>
                                f.get("paidBillDetails").contentAccountRef.contentAccountRef === _bill.get("contentAccountRef").contentAccountRef
                                && f.get("paidBillDetails").contentAccountRef.contentLoginRef === _bill.get("contentAccountRef").contentLoginRef
                            );
                        } catch (e) {
                            if (calendarGrouping.selectedMonth !== calendarGrouping.currentMonth) return retVal;
                            if (!messages.show) {
                                if (!window.MintOil.datePickerReloaded) {
                                    $.datepicker._adjustDate('#leftColCal', 0, "M");
                                    window.MintOil.datePickerReloaded = true;
                                }
                                logger.warn({handled_rejection: e});
                                messages.show = true;
                                $("#messages").html(`<div class="error">Payments Service in Mint seems to be down.<br/>Functionality on this page <em>may</em> be showing incorrect data.</div>`);
                            }
                        }

                        retVal = retVal.map(payment => {
                            return {
                                "id": payment.get("id"),
                                "amount": payment.get("amount"),
                                "currency": payment.get("currency"),
                                "status": payment.get("status"),
                                "details": payment.get("paidBillDetails"),
                                "dueDate": payment.get("paidBillDetails").dueDate
                            }
                        });

                        retVal.getByDate = (date) => {
                            return this.filter(m => m.get("paidBillDetails").dueDate === date);
                        };

                        return {
                            getByDate: (date) => {
                                return retVal.find(m => moment(m.dueDate).format("YYYY-MM") === moment(date).format("YYYY-MM")) || null;
                            },
                            collection: retVal
                        };
                    })(_bill, receipts);

                    // "Notes" is a little tricky, so I want to be concise and not have the silly thing repeat itself.
                    if (_bill.getListDisplayStatus().toLowerCase() !== "due") {
                        if (_bill.getListDisplayStatus().toLowerCase().indexOf("full") > 0) {
                            notes = _bill.getListDisplayStatus().replace("Paid Full", "Paid in Full");
                        } else {
                            notes = `<div>${_bill.getDisplayStatus("module")}</div>`;
                        }
                    } else {
                        notes = ``;
                    }

                    amountDueMin = (typeof _bill.get("minimumPaymentDue") !== "undefined") ? `${_bill.get("minimumPaymentDue").toCurrencyString()}` : 0.00;
                    amountPaid = _bill.getAmountPaid() ? parseFloat(_bill.getAmountPaid().replace("$", "")) : 0.00;

                    if (_bill.isNow) {
                        _bill.isFuture = false;
                        _bill.isPast = false;

                        if (moment(_bill.get("dueDate")).format("YYYY-MM") !== calendarGrouping.currentMonthGrouping) {
                            let payment = payments.getByDate(calendarGrouping.selectedMonthGrouping);
                            if (payments.collection.length > 0 && payments.getByDate(calendarGrouping.currentMonthGrouping)) {
                                amountDue = payment.details.dueAmount;
                                amountPaid = payment.amount;
                                amountDueMin = payment.details.hasOwnProperty("minimumPaymentDue") ? payment.details.minimumPaymentDue : amountDue;
                                if (payment.details.hasOwnProperty("dueDate")) {
                                    _bill.dueDate = moment(payment.details.dueDate).format("MM-DD");
                                }
                            }
                        }
                    } else {
                        if (_bill.isFuture) {
                            amountDue = _bill.dueAmount ? _bill.dueAmount.toCurrencyString() : 0.00.toCurrencyString();
                            amountPaid = 0.00;
                        } else if (_bill.isPast) {
                            let payment = payments.getByDate(calendarGrouping.selectedMonthGrouping);
                            if (payments.collection.length > 0 && payments.getByDate(calendarGrouping.selectedMonthGrouping)) {
                                amountDue = payment.details.dueAmount;
                                amountPaid = payment.amount;
                                amountDueMin = payment.details.hasOwnProperty("minimumPaymentDue") ? payment.details.minimumPaymentDue : amountDue;
                            }
                        }
                    }

                    return {
                        "contentAccountRef": _bill.get("contentAccountRef"),
                        "bill": _bill.getTitle(),
                        "due": amountDue.toCurrencyString() || 0.00.toCurrencyString(),
                        "aggDue": _bill.get("aggregationDueAmount"),//.toLocaleString("en-US", formatOptions),
                        "min": amountDueMin.toCurrencyString(),
                        "date_due": moment(_bill.dueDate || _bill.get("dueDate") || _bill.getLatestDeliveryDate()).format("MM-DD"),
                        "date_bd": ((_bill, moment) => {
                            let date_due = moment(_bill.getLatestDeliveryDate() || _bill.get("dueDate"));
                            return {
                                "month": date_due.format("MM"),
                                "day": date_due.format("DD")
                            }
                        })(_bill, moment),
                        "id": _bill.id,
                        "model": _bill.model,
                        "payments": payments,
                        "isPaid": _bill.isFuture ? false : _bill.isPaid(),
                        "isPaidInFull": notes.toLowerCase() === "paid in full",
                        "isAutopaid": _bill.isAutopaid(),
                        "billStatus": _bill.get("billStatus"),
                        "amountPaid": amountPaid.toCurrencyString(),
                        "isFullyScheduled": _bill.isFullyScheduled(),
                        "schedulePaid": _bill.getAmountScheduled(),
                        "monthGroup": _bill.getYearMonthGroup(),
                        "billingPattern": ((bill) => {
                            const defaultRetVal = {frequency: "MONTH", repeatInterval: 1};
                            let instType = bill.get("institutionType");
                            if (instType === "LINKED") return defaultRetVal;
                            let biller = bill.getBiller();
                            if (!biller) return defaultRetVal;
                            return {
                                frequency: biller.get("frequency"),
                                repeatInterval: biller.get("repeatInterval")
                            };
                        })(_bill),
                        "formattedNotes": _bill.isFuture ? "" : notes,
                        "details": _bill.details,
                        "notes": _bill.isFuture ? "" : _bill.getDisplayStatus("module"),
                        "notes2": _bill.isFuture ? "" : _bill.getListDisplayStatus()
                    };
                });

                bills = bills.map(bill => {
                    // noinspection JSUnusedLocalSymbols
                    let today = new Date(),
                        currentDate = moment(today).format("DD"),
                        currentMonth = moment(today).format("MM");
                    /*let billDate = bill.date_bd.day,
                        billMonth = bill.date_bd.month;

                    if (billMonth !== currentMonth) {
                        bill.date_due = `${currentMonth}-${billDate}`;
                    }
                    */
                    /*bill.formattedNotes = "";
                    bill.notes = "";
                    bill.notes2 = "";*/

                    return bill;
                });

                bills = _.sortBy(bills, bill => bill.date_bd.day);

                if (localStorage.getItem("env") === "debug") {
                    logger.info({"bills": bills});
                }

                window.MintOil.Bills = bills;

                callback({
                    recordsTotal: bills.length,
                    recordsFiltered: bills.length,
                    data: bills
                });

                bills.forEach((bill) => {
                    if (bill.formattedNotes.length === 0) return;
                    new MintPopoverView({
                        "width": "400px",
                        "align": "rightTop",
                        "trigger": "hoverToggle",
                        "toggleWith": $(`#bill-tac-calc_bill_${bill.id}`),
                        "contentHeading": `${bill.bill} - {{{lang.exports.notes}}}`,
                        "contentBody": `${bill.formattedNotes}`,
                        "x": false
                    });
                });

                initCheckbox();
            }

            //#endregion

            //#region Input Events
            // Have to make this a function, because we'll need to re-run it whenever the calendar changes months.
            const initInputEvents = () => {
                let $currencyInputs = $(".calc_custom_currency:not([contenteditable=false])");

                $currencyInputs.off();

                $currencyInputs.on("keydown", (event) => {
                    let newValue = $(event.target).html();
                    let regex = /^\d+\.?\d{0,2}$/gs;
                    if (regex.test(newValue) === false) {
                        event.preventDefault();
                        // noinspection JSCheckFunctionSignatures
                        $(event.target).html(newValue.localStringToNumber().toCurrencyString(FORMAT_OPTIONS_INPUT).replace(",", ""));
                    }

                    if (event.keyCode === 13) {
                        event.preventDefault();
                        $.tabNext();
                    }

                    if (newValue.length >= 9) {
                        let valNum = newValue.localStringToNumber();
                        if (valNum >= 999999.99) {
                            event.preventDefault();
                            $(event.target).html(0.00.toCurrencyString());
                            new MintPopoverView({
                                "trigger": true,
                                "extraClass": "skinny",
                                "align": "leftCenter",
                                "width": "238px",
                                "pop": true,
                                "toggleWith": $(event.target),
                                "contentBody": `Value cannot be over 999,999.99`,
                                "type": "error",
                                "x": false,
                                "onClose": function () {
                                    let that = this;
                                    that.remove();
                                },
                                "onShow": function () {
                                    let that = this;
                                    setTimeout(() => {
                                        that.xClose(new MouseEvent("click", {
                                            bubbles: true,
                                            cancelable: true,
                                            view: window
                                        }));
                                    }, 2500);
                                }
                            }).showToggle();
                        }
                    }

                    let shifted = event.shiftKey;

                    let onMinus = () => {
                        $(event.target).html($(event.target).data("min"));
                        event.preventDefault();
                    };
                    let onEqual = () => {
                        let self = $(event.target);
                        if (self.data("subtracted")) {
                            event.preventDefault();
                            return;
                        }
                        let paid = self.data("paid").localStringToNumber();
                        let calc = self.html().localStringToNumber();
                        let retVal = 0.00;
                        if (paid > 0) {
                            retVal = calc - paid;
                            self.data("subtracted", true);
                        }
                        $(event.target).html(retVal.toCurrencyString());
                        event.preventDefault();
                    };
                    let onPlus = () => {
                        if (shifted) return onEqual();
                        $(event.target).html($(event.target).data("full"));
                        event.preventDefault();
                    };

                    if (event.keyCode === 187) {
                        if (shifted) {
                            onPlus();
                            return;
                        } else {
                            onEqual();
                            return;
                        }
                    }

                    if (event.keyCode === 107) {
                        onPlus();
                        return;
                    }

                    if (event.keyCode === 189 || event.keyCode === 109) {
                        onMinus();
                        return;
                    }

                    if ((event.keyCode >= 48 && event.keyCode <= 57)
                        || (event.keyCode >= 96 && event.keyCode <= 105)
                        || event.keyCode === 8
                        || event.keyCode === 9
                        || event.keyCode === 36
                        || event.keyCode === 35
                        || event.keyCode === 37
                        || event.keyCode === 39
                        || event.keyCode === 46
                        || event.keyCode === 190
                        || event.keyCode === 110) {
                        // Allow it...
                        $(event.target).data('subtracted', false);
                    } else {
                        event.preventDefault();
                    }

                    // If a decimal is already been placed, disable a second one.
                    if (newValue.indexOf(".") !== -1 && event.keyCode === 190 || newValue.indexOf(".") !== -1 && event.keyCode === 110) {
                        event.preventDefault();
                    }
                });

                $(window).blur(function () {
                    window.getSelection().removeAllRanges();
                });
                $(window).focus(function () {
                    setTimeout(function () {
                        window.getSelection().removeAllRanges();
                    }, 20);
                    window.getSelection().removeAllRanges();
                });

                $currencyInputs.on("focus", function (e) {
                    let newValue = $(e.target).html();

                    setTimeout(function () {
                        document.execCommand('selectAll', false, null);
                    }, 10);

                    // noinspection JSCheckFunctionSignatures
                    $(e.target).html(newValue
                        ? newValue.localStringToNumber().toCurrencyString(FORMAT_OPTIONS_INPUT).replace(",", "")
                        : "");
                    $(e.target).select();
                    $(e.target).parents("tr").addClass("hot-track");
                    $(e.target).parents("td").siblings(".col-break").addClass("uncol-break").removeClass("col-break");
                });

                $currencyInputs.on("blur", (e) => {
                    let $self = $(e.target),
                        newValue = $self.html(),
                        calcValue;

                    calcValue = newValue ? newValue.localStringToNumber().toCurrencyString() : "";
                    calcStore.upsert(e.target.id, calcValue.localStringToNumber()).catch(err => logger.error(err));

                    $(e.target).parents("tr").removeClass("hot-track");
                    $(e.target).parents("td").siblings(".uncol-break").addClass("col-break").removeClass("uncol-break");
                    $(e.target).html(calcValue);

                    $("#ideal-payment").html(calcIdealPayment().toCurrencyString());
                    window.getSelection().removeAllRanges();
                });

                //#region Column Operations
                let runStoreCalcProc = () => {
                    let calcSave = {};
                    $currencyInputs.each(function () {
                        let $self = $(this)[0];
                        calcSave[$self.id] = $($self).html().localStringToNumber();
                    });
                    calcStore.setRange(calcSave, calendarGrouping.selectedMonthGrouping).catch(err => logger.error(err));
                };

                let $btnTransposeDue = $("#btn-transpose-due");
                $btnTransposeDue.off();
                $btnTransposeDue.on("click", () => {
                    window.MintOil.Bills.forEach(bill => {
                        $(`#calc_custom_${bill.id}_${bill.date_due}`).html(bill.due.localStringToNumber().toCurrencyString());
                    });
                    $("#ideal-payment").html(calcIdealPayment().toCurrencyString());
                    runStoreCalcProc();
                });

                let $btnTransposeMin = $("#btn-transpose-min");
                $btnTransposeMin.off();
                $btnTransposeMin.on("click", () => {
                    window.MintOil.Bills.forEach(bill => {
                        $(`#calc_custom_${bill.id}_${bill.date_due}`).html(bill.min ? bill.min : bill.due.localStringToNumber().toCurrencyString());
                    });
                    $("#ideal-payment").html(calcIdealPayment().toCurrencyString());
                    runStoreCalcProc();
                });

                let $btnTransposePaid = $("#btn-transpose-paid");
                $btnTransposePaid.off();
                $btnTransposePaid.on("click", () => {
                    window.MintOil.Bills.forEach(bill => {
                        let $elem = $(`#calc_custom_${bill.id}_${bill.date_due}`);
                        if ($elem.data("subtracted")) return;
                        if ($elem.html().localStringToNumber() > 0) {
                            $elem.html(
                                ($elem.html().localStringToNumber() - bill.amountPaid.localStringToNumber()).toCurrencyString()
                            );
                            $elem.data("subtracted", true);
                        }
                    });
                    $("#ideal-payment").html(calcIdealPayment().toCurrencyString());
                    runStoreCalcProc();
                });

                let $btnClearIdeal = $("#btn-clear-ideal");
                $btnClearIdeal.off();
                $btnClearIdeal.on("click", () => {
                    window.MintOil.Bills.forEach(bill => {
                        $(`#calc_custom_${bill.id}_${bill.date_due}`)
                            .data("subtracted", false)
                            .html(0.00.toCurrencyString());
                    });
                    $("#ideal-payment").html(calcIdealPayment().toCurrencyString());
                    runStoreCalcProc();
                });


                let $colOp = $(".col-op");
                $colOp.hover(function () {
                    $(".click-focus, th.click-focus button").addClass("track");
                    $(this).find(".fa").addClass("fa-chevron-right");

                    let text = $(this).data('text');
                    $("#btn-clear-ideal").html(`<i class="fa fa-chevron-down left"></i>${text}<i class="fa fa-chevron-down"></i>`);
                }, function () {
                    let $btnClearIdeal = $("#btn-clear-ideal");
                    let text = $btnClearIdeal.data('text');
                    $btnClearIdeal.html(`${text}`);
                    $(this).find(".fa").removeClass("fa-chevron-right");
                    $(".click-focus, th.click-focus button").removeClass("track");
                });
                //#endregion
            };
            //#endregion

            //#region Bill DataTable
            // noinspection JSUnusedLocalSymbols
            let monthlyBillsTable = $("#monthlyBillsTable").DataTable({
                "searching": false,
                "ordering": false,
                "info": false,
                "paging": false,
                "processing": true,
                "serverSide": true,
                "autoWidth": false,
                "columns": [
                    {
                        "data": "bill",
                        "className": "tdBill",
                        "render": (data, type, row) => {
                            let notes = ``;
                            if (row.formattedNotes.length !== 0) {
                                notes = `<a id="bill-tac-calc_bill_${row.id}" class="icon icon-info bill-calc-note" style="cursor:help;" data-header="Notes">&nbsp;</a>`;
                            } else {
                                notes = `<a class="icon" style="cursor:inherit;"></a>`;
                            }
                            return `<div class="tdBillWrapper" title="${data}">${data}</div>${notes}`;
                        }
                    },
                    {
                        "data": "isAutopaid",
                        "type": "html",
                        "className": "",
                        "render": (data) => (data) ? `<div class="autopay-check">&#10003;</div>` : ``
                    },
                    {
                        "data": "due",
                        "type": "num-fmt",
                        "className": "currency"
                    },
                    {
                        "data": "min",
                        "type": "num-fmt",
                        "className": "currency",
                        "render": (data, type, row) => (data || false) ? (data.localStringToNumber() === 0 ? row.due : data) : row.due
                    },
                    {
                        "data": "amountPaid",
                        "type": "num-fmt",
                        "className": "currency",
                        "createdCell": (td, cellData) => {
                            if (cellData > 0.00) {
                                $(td).addClass("calc_paid");
                            } else {
                                $(td).removeClass("calc_paid");
                            }
                        }
                    },
                    {
                        "data": "date_due",
                        "type": "date",
                        "className": "date col-break"
                    },
                    {
                        "type": "html",
                        "className": "currency click-focus",
                        "render": (data, type, row, meta) => {
                            calcStore.get(`calc_custom_${row.id}_${row.date_due}`, calendarGrouping.selectedMonthGrouping)
                                .then(value => {
                                    if (value !== null) {
                                        let $current_custom_field = $(`#calc_custom_${row.id}_${row.date_due}`);
                                        $current_custom_field.html(value.toCurrencyString());
                                        $current_custom_field.trigger('blur');
                                    }
                                });
                            return `<span
                                tabindex="${!row.isPaidInFull ? meta["row"] + 1 : -1}"
                                class="calc_custom_currency"
                                id="calc_custom_${row.id}_${row.date_due}"
                                data-full="${row.due.localStringToNumber() || 0.00}"
                                data-min="${row.min.localStringToNumber() || 0.00}"
                                data-paid="${row.amountPaid.localStringToNumber() || 0.00}"
                                contenteditable="${!row.isPaidInFull}">0.00</span>`;
                        }
                    },
                    {
                        "type": "num-fmt",
                        "className": "currency",
                        "render": (data, type, row, meta) => {
                            let calcPayment = 0.00;

                            if (row.billStatus !== "FULL") {
                                if (row.model === "CREDIT_CARD" || row.model === "LOAN") {


                                    if (row.details !== null && row.details.purchasesApr !== null) {
                                        calcPayment = (((row.details.purchasesApr * 0.01) * row.details.availableBalanceAmount) + row.details.availableBalanceAmount) / 12;
                                    } else if (row.payments.collection.length > 0) {
                                        calcPayment = row.payments.collection.map(p => p.details.minimumPaymentDue || p.details.dueAmount).reduce((a, b, i, arr) => a + b / arr.length);
                                    }

                                    if (isNaN(calcPayment)) {
                                        calcPayment = row.min.localStringToNumber() ? row.min.localStringToNumber() : row.due.localStringToNumber();
                                    }
                                }else{
                                    calcPayment = (row.min || false) ? (row.min.localStringToNumber() === 0 ? row.due : row.min.localStringToNumber()) : row.due;
                                }
                            }

                            return `<span class="calc_payment_currency" id="calc_payment_${row.id}_${row.date_due}">${calcPayment.toCurrencyString()}</span>`;
                        }
                    },
                ],
                "order": [[4, "desc"]],
                "ajax": getDataTableData,
                "drawCallback": function () {
                    initInputEvents();
                },
                "footerCallback": function (row, data, start, end, display) {
                    let api = this.api();

                    /** @type {number} */
                    let totalDue = api
                        .column(2)
                        .data()
                        .reduce((a, b) => a.localStringToNumber() + b.localStringToNumber(), 0);

                    /** @type {number} */
                    let totalMinPayment = api
                        .column(3)
                        .data()
                        .reduce((a, b) => a.localStringToNumber() + b.localStringToNumber(), 0);

                    /** @type {number} */
                    let totalPaid = api
                        .column(4)
                        .data()
                        .reduce((a, b) => a.localStringToNumber() + b.localStringToNumber(), 0);

                    /* Update footer */
                    $(api.column(2).footer()).html(`${totalDue.toCurrencyString()}`);
                    $(api.column(3).footer()).html(`${totalMinPayment.toCurrencyString()}`);
                    $(api.column(4).footer()).html(`${totalPaid.toCurrencyString()}`);
                    $(api.column(6).footer()).html(`${calcIdealPayment().toCurrencyString()}`);

                    /* let $tableFooter = $('#monthlyBillsTable tfoot');
                     if ($tableFooter.find(".bank-account").length === 0){
                         $tableFooter.append(`
<tr class="bank-account">
 <th colspan="10">Test 1234</th>
</tr>`);
                     }*/

                },
                "createdRow": (row, /** MintOil.Bill*/ data) => {
                    if (data.isPaidInFull) {
                        $(row).addClass('paid-in-full');
                    }
                }
            });
            //#endregion

            //#region Mint Date-Picker override
            // noinspection JSJQueryEfficiency
            let $currentDatePicker = calendarGrouping.DatePicker;
            let baseDatePickerFunction = $currentDatePicker.settings.onChangeMonthYear;
            $currentDatePicker.settings.onChangeMonthYear = (y, m) => {
                baseDatePickerFunction(y, m);
                monthlyBillsTable.ajax.reload();
            };
            //#endregion

            //#region Options button
            let $btnOptions = $("#options");
            // noinspection JSUnusedLocalSymbols
            let optionsTooltip = new MintPopoverView({
                "width": "90px",
                "align": "leftTop",
                "trigger": "hoverToggle",
                "toggleWith": $btnOptions,
                "contentBody": "Options",
                "extraClass": "skinny"
            });
            // noinspection JSUnusedLocalSymbols
            let $btnOptionsPopup = new MintPopoverView({
                "width": "420px",
                "align": "leftTop",
                "trigger": "clickToggle",
                "toggleWith": $btnOptions,
                "contentHeading": `MintOil Monthly Calculator - Options`,
                "contentBody": $("#bill-calc-options").html(),
                "x": true,
                "enableClickOffClose": false,
                "onShow": () => {
                    initCheckbox();
                }
            });

            let $btnExport = $("#export");
            new MintPopoverView({
                "width": "248px",
                "align": "leftTop",
                "trigger": "hoverToggle",
                "toggleWith": $btnExport,
                "contentBody": "Export current view to a CSV file&nbsp;",
                "extraClass": "skinny"
            });
            $btnExport.on("click", () => {
                window.MintOil.ExportBillsToCSV();
            });
            //#endregion

            //#region CSV Export
            window.MintOil.ExportBillsToCSV = () => {
                let output = MintOil.Bills.map((v) => {
                    let renameKeys = (keysMap, obj) => Object
                        .keys(obj)
                        .reduce((acc, key) => ({
                            ...acc,
                            ...{[keysMap[key] || key]: obj[key]}
                        }), {});

                    let clone = Object.assign({}, v);
                    clone.bill = `"${clone.bill}"`;
                    clone.due = `$${clone.due.localStringToNumber().toFixed(2)}`;
                    // noinspection JSValidateTypes
                    clone.min = `$${clone.min.localStringToNumber().toFixed(2)}`;
                    clone.amountPaid = `$${clone.amountPaid.localStringToNumber().toFixed(2)}`;
                    clone.schedulePaid = `$${clone.schedulePaid.localStringToNumber().toFixed(2)}`;
                    clone.notes = clone.notes2.toLowerCase().trim() === "due" ? `"Due on ${clone.notes.trim()}"` : `"${clone.notes.trim()}"`;


                    delete clone.contentAccountRef;
                    delete clone.id;
                    delete clone.aggDue;
                    delete clone.date_bd;
                    delete clone.payments;
                    delete clone.billStatus;
                    delete clone.formattedNotes;
                    delete clone.billingPattern;
                    delete clone.monthGroup;
                    delete clone.notes2;
                    delete clone.details;

                    let keysMap = {
                        bill: "{{{lang.bill-name}}}",
                        due: "{{{lang.payments.full}}}",
                        min: "{{{lang.payments.min}}}",
                        date_due: "{{{lang.date-due}}}",
                        isAutopaid: "{{{lang.exports.is.autopaid}}}",
                        isPaid: "{{{lang.exports.is.paid}}}",
                        isPaidInFull: "{{{lang.exports.is.paid-in-full}}}",
                        amountPaid: "{{{lang.payments.paid}}}",
                        isFullyScheduled: "{{{lang.exports.is.fully-scheduled}}}",
                        schedulePaid: "{{{lang.exports.scheduled}}}",
                        notes: "{{{lang.exports.notes}}}"
                    };

                    if (mintOilCalcPageOptions._exportInputColumns === true) {
                        let $idealPayment = $(`#calc_custom_${v.id}_${v.date_due}`);
                        let $calculatedPayment = $(`#calc_payment_${v.id}_${v.date_due}`);
                        let originalNotesValue = clone.notes;
                        delete clone.notes;

                        clone.idealPayment = $idealPayment.html() ? `$${$idealPayment.html().localStringToNumber().toFixed(2)}` : `$${0.00.toCurrencyString()}`;
                        keysMap.idealPayment = `{{{lang.payments.ideal}}}`;

                        clone.calculatedPayment = $calculatedPayment.html() ? `$${$calculatedPayment.html().localStringToNumber().toFixed(2)}` : `$${0.00.toCurrencyString()}`;
                        keysMap.calculatedPayment = "{{{lang.payments.rec}}}";

                        clone.notes = originalNotesValue;
                    }

                    return renameKeys(keysMap, clone);
                }).toCSV();
                let $datepicker = calendarGrouping.DatePicker;
                let monthName = moment($datepicker.selectedMonth + 1, 'MM').format('MMMM');
                let blob = new Blob([output], {type: "text/csv;charset=utf-8"});
                window.saveAs(blob, `${monthName} (${$datepicker.selectedYear}) Bills.csv`);
            };
            //#endregion
        });
})(require);