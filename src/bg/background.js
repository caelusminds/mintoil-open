(function () {
    const IS_DEBUG = localStorage.getItem('env') === 'debug';
    const TRIAL_PERIOD_DAYS = IS_DEBUG ? 90 : 90;

    // noinspection JSValidateTypes
    /** @type {FileEntry[]|DirectoryEntry[]} Package Files*/
    let package_files = {};

    // Routine for loading the package files into the above local.
    chrome.runtime.getPackageDirectoryEntry(directoryEntry => {
        function readDirRecursive(dir, callback) {
            const rootPath = dir.fullPath;
            const subDirs = [];

            function readDir(dir, files) {
                files = files || [];
                const reader = dir.createReader();
                reader.readEntries(entries => {
                    Array.from(entries).forEach(entry => {
                        if (
                            entry.name.startsWith(`.`) ||
                            entry.name.startsWith(`node_`) ||
                            entry.name.startsWith(`scripts`)
                        ) {
                            return;
                        } else {
                            let relPath = entry.fullPath.replace(rootPath, "");
                            entry.relativePath = relPath;
                            entry.extensionPath = `chrome-extension://${chrome.runtime.id}${relPath}`;
                        }
                        if (entry.isDirectory) {
                            subDirs.push(entry);
                        } else {
                            files.push(entry);
                        }
                    });
                    if (!subDirs.length) {
                        return callback(files);
                    }
                    readDir(subDirs.pop(), files);
                });
            }

            readDir(dir);
        }

        readDirRecursive(directoryEntry, entries => {
            entries.map(f => package_files[f.name.replace(`.min.`, `.`)] = f);
        });
    });

    /**
     * Current Google User.
     * @class MintOil.CurrentUser
     * @property {string} id Google user ID.
     * @property {string} email Google User email.
     * @property {boolean} verified_email Has user verified email?
     * @property {string} name Google User full name.
     * @property {string} given_name Google user first name.
     * @property {string} family_name Google user last name.
     * @property {string} picture Google user avatar URL.
     * @property {string} locale Google user locale.
     * @property {string} [hd] Google user Domain.
     * @property {string} [token] Auth Token.
     */
    MintOil.CurrentUser = null;

    MintOil.LoginUser = () => {
        return new Promise((resolve, reject) => {
            chrome.identity.getAuthToken({
                interactive: true
            }, (token) => {
                if (chrome.runtime.lastError) {
                    MintOil.Logger.error(chrome.runtime.lastError.message);
                    reject(chrome.runtime.lastError.message);
                    return;
                }

                let xhr = new XMLHttpRequest();
                xhr.open('GET', 'https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=' + token);
                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) {
                        if (xhr.status >= 200 && xhr.status < 300) {
                            MintOil.Logger.info("Login Response", xhr.response);
                            MintOil.CurrentUser = JSON.parse(xhr.response);
                            MintOil.CurrentUser.token = token;
                            MintOil.Logger.info("Final MintOil Object", MintOil);
                            MintOil.Logger.debug({token: token, user: MintOil.CurrentUser});
                        }
                    }
                };
                xhr.send();
            });
        });
    };

    MintOil.Listeners = {
        /**
         * On chrome.runtime.OnMessage()
         * @param {string|*} message The message sent by the calling script.
         * @param {chrome.runtime.MessageSender} sender The message sender.
         * @param {function} sendResponse Function to call when there is a response. Return Object, will jsonify.
         */
        onMessage: (message, sender, sendResponse) => {
            MintOil.Logger.debug("Incoming message", {message, sender});
            switch (message.command) {
                case "getPackageFiles":
                    sendResponse(package_files);
                    break;
                case "checkLicense":
                    sendResponse(__license === null ? null : {status: __license, days_left: __daysLeft});
                    return false;
                case "checkLicenseRefresh":
                    sendResponse(true);
                    break;
                case "closedTrialPrompt":
                    break;
            }
        },
        pageAction: {
            onClick: () => {
                chrome.tabs.query(
                    {
                        url: [
                            "*://mint.intuit.com/*",
                            "*://accounts.intuit.com/*"]
                    }, (matches) => {
                        if (matches.length > 0) {
                            chrome.tabs.highlight({tabs: [matches[0].index]}, () => {
                                MintOil.Logger.log("Tab match found.", {tab: [matches[0]]});
                            });
                        } else {
                            chrome.tabs.create({
                                active: true,
                                selected: true,
                                url: "https://mint.intuit.com/mintoil.event"
                            }, (e) => {
                                MintOil.Logger.log("Clicked", {e});
                            });
                        }
                    });
            }
        },
        webNavigation: {
            COMMON_URL_FILTERS: {
                url: [
                    {hostSuffix: 'mint.intuit.com'}
                ]
            },
            onBeforeNavigate: (info) => {
                MintOil.Logger.log('before navigation ', {info});

                let cutoff = info.url.indexOf('/mintoil.event');
                if (cutoff >= 0) {
                    chrome.tabs.update(info.tabId, {url: info.url.substring(0, cutoff) + '/overview.event#mintoil-override'}, (e) => {
                        MintOil.Logger.info({e});
                    });
                }
            },
            onCompleted: (info) => {
                MintOil.Logger.log('on completed ', {info});

                if (!MintOil.CurrentUser) {
                    MintOil.LoginUser();
                }

                if (info.url.indexOf('#mintoil-override') >= 0) {
                    chrome.tabs.sendMessage(info.tabId, {command: 'navto-mintoil'});
                }

                if (info.url.indexOf('/bills.event') >= 0) {
                    chrome.tabs.sendMessage(info.tabId, {command: 'show-ical-button'});
                }

                if (info.url.indexOf('/transaction.event') >= 0) {
                    chrome.tabs.sendMessage(info.tabId, {command: 'load-transaction-page'});
                }
            }
        },
        notifications: {
            openChromeStore: (meta) => {
                chrome.tabs.create({
                    active: true,
                    selected: true,
                    url: `https://chrome.google.com/webstore/detail/${chrome.runtime.id}/`
                }, (e) => {
                    MintOil.Logger.log("Opening new tab to chrome store...", {meta});
                });
            },
            onClicked: (/**string*/notificationId) => {
                MintOil.Logger.log("OnClicked", {notificationId});
            },
            onButtonClicked: (/**string*/notificationId, /**number*/buttonIndex) => {
                MintOil.Logger.log("onButtonClicked", {notificationId, buttonIndex});
            }
        }
    };

    chrome.browserAction.onClicked.addListener(MintOil.Listeners.pageAction.onClick);
    chrome.webNavigation.onBeforeNavigate.addListener(MintOil.Listeners.webNavigation.onBeforeNavigate, MintOil.Listeners.webNavigation.COMMON_URL_FILTERS);
    chrome.webNavigation.onCompleted.addListener(MintOil.Listeners.webNavigation.onCompleted, MintOil.Listeners.webNavigation.COMMON_URL_FILTERS);

    chrome.runtime.onMessage.addListener(MintOil.Listeners.onMessage);

    chrome.notifications.onClicked.addListener(MintOil.Listeners.notifications.onClicked);
    chrome.notifications.onButtonClicked.addListener(MintOil.Listeners.notifications.onButtonClicked);

})();
