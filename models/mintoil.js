/**
 * @class MintOilAccount
 * @property {string} name Account name.
 * @property {number} accountNumber Last four account numbers.
 * @property {boolean} isActive
 * @property {boolean} isVisible
 * @property {boolean} isBillVisible
 * @property {number} availableBalance
 * @property {string} bankName
 * @property {'SAVINGS'|'CHECKING'|string} bankAccountType
 */

// noinspection JSUnusedGlobalSymbols
/**
 * A simple class for handling the data to and from Intuit® Mint™
 * @class {MintOil}
 */
let MintOil;
MintOil = new class {
    /**
     * Gets bills returned from Intuit® Mint™
     * @returns {Array|*}
     */
    get Bills() {
        return this._bills;
    }

    /**
     * Sets bills returned from Intuit® Mint™
     * @param {Array|*} bills
     */
    set Bills(bills) {
        if (bills) {
            this._bills = bills;
        }
    }

    /**
     * Gets providers returned from Intuit® Mint™
     * @returns {Array|*}
     */
    get Providers() {
        return this._providers;
    }

    /**
     * Sets providers returned from Intuit® Mint™
     * @param {Array|*} providers
     */
    set Providers(providers) {
        if (providers) {
            this._providers = providers;
        }
    }

    /**
     * Gets receipts from Intuit® Mint™
     * @returns {Array|*}
     */
    get Receipts() {
        return this._receipts;
    }

    /**
     * Sets receipts from Intuit® Mint™
     * @param {Array|*} receipts
     */
    set Receipts(receipts) {
        if (receipts) {
            this._receipts = receipts;
        }
    }

    /**
     * Gets the total amount of liquid assets the current user has.
     * @return {number}
     */
    get TotalLiquidity() {
        return this._cash;
    }

    /**
     * Sets the total amount of liquid assets the current user has.
     * @param {number} value
     *
     */
    set TotalLiquidity(value) {
        if (value) {
            this._cash = value;
        }
    }

    /**
     * Gets the Google Authentication token
     * @returns {string}
     */
    get GoogleAuthToken() {
        return this._googleAuthToken;
    }

    /**
     * Sets the Google Authentication token
     * @param {string} token
     */
    set GoogleAuthToken(token) {
        this._googleAuthToken = token;
    }

    /**
     * Gets the Main Window ID
     * @returns {int} Window ID
     */
    get MainWindowID() {
        return this._mainWindowID;
    }

    /**
     * Sets the Main Window ID
     * @param {int} id Window ID
     */
    set MainWindowID(id) {
        this._mainWindowID = id;
    }

    /**
     * Gets the timestamp the data was last loaded
     * @returns {timestamp|int}
     */
    get LastLoaded() {
        return this._lastLoaded;
    }

    /**
     * Imports data from Intuit® Mint™
     * @function
     * @param {Logger|*} objectIn
     */
    ImportData(objectIn) {
        let self = this;

        if (!objectIn) {
            return;
        }

        // Loop through our properties
        for (let property in objectIn) {
            if (property in this) {
                // noinspection JSUnfilteredForInLoop
                self.Logger.log(`Imported ${property} successfully.`);
                // noinspection JSUnfilteredForInLoop
                this[property] = objectIn[property]
            }
        }

        this._lastLoaded = Date.now();
    }

    /**
     * MintOil constructor
     * @constructor
     */
    constructor() {
        // noinspection JSUnusedLocalSymbols
        window.Logger.useDefaults({
            formatter: (messages, context) => {
                // prefix each log message with a timestamp.
                messages.splice(0, 0, `%c${MINTOIL_CONSTANTS.application.name}`);
                messages.splice(1, 0, MINTOIL_CONSTANTS.logger.style);
            }
        });

        const env = localStorage.getItem('env');
        this.Logger = window.Logger.get(MINTOIL_CONSTANTS.application.name);
        if (env !== "debug") {
            this.Logger.setLevel(window.Logger.ERROR);
        } else {
            this.Logger.setLevel(window.Logger.TRACE);
        }

        this._lastLoaded = 0;
        this._mainWindowID = 0;
        this._bills = {};
        this._providers = {};
        this._receipts = [];
        this._cash = 0.00;
    }
}();

