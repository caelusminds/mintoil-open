#!/usr/bin/env node
const path = require("path");
const fs = require('fs');
const archiver = require('archiver');
const glob = require('glob');

const csso = require('csso');
const uglifyjs = require('uglify-es');
const manifest = require('./../manifest.json');
const config = require('dotenv').config();

let cwd = __dirname + '/../';
let dir = '.builds';
cwd = cwd.replace(/\\/gi, '/');

if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

// create a file to stream archive data to.
let output = fs.createWriteStream(`${cwd}.builds/mintoil-v${manifest.version}.zip`);
let archive = archiver('zip', {
    zlib: {level: 9} // Sets the compression level.
});

// listen for all archive data to be written
// 'close' event is fired only when a file descriptor is involved
output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
});

// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
output.on('end', function () {
    console.log('Data has been drained');
});

// good practice to catch warnings (ie stat failures and other non-blocking errors)
archive.on('warning', function (err) {
    if (err.code === 'ENOENT') {
        // log warning
    } else {
        // throw error
        throw err;
    }
});

// good practice to catch this error explicitly
archive.on('error', function (err) {
    throw err;
});

// pipe archive data to the file
archive.pipe(output);

// Get manifest file stream...
let manifestFileStream = fs.readFileSync(cwd + 'manifest.json', 'utf8');

// Append Images
console.log("Appending images directory...");
archive.directory(cwd + 'images/', 'images');

// Append CSS libs and Fonts
console.log("Appending css sub-directories...");
archive.directory(cwd + 'css/images/', 'css/images');
archive.directory(cwd + 'css/fonts/', 'css/fonts');

// Append Locales
console.log("Appending locales...");
archive.directory(cwd + '_locales/', '_locales');

// Append Schema from the models dir
console.log("Appending schema...");
archive.file(cwd + 'models/schema.json', {name: 'schema.json', prefix: 'models'});

// Keeping it DRY
Array.prototype.sanitize = function () {
    return this
        .filter(f => f.indexOf('/../node_modules/') === -1)
        .filter(f => f.indexOf('/../.mint-lib/') === -1)
        .filter(f => f.indexOf('/../.scripts/') === -1)
        .map(m => m.replace(cwd, ''));
};

// Minfy and Append JS Files
console.log("\nStarting the JS files...");
let jsFiles = glob(`${cwd}/**/*.js`, {sync: true})
    .sanitize()
    .forEach(f => {
        // Ignore manifest.js
        if (f.indexOf('manifest.js') > -1) return;

        console.log(`Archiving ${f}...`);
        if (f.indexOf('.min.') === -1) {
            let file = fs.readFileSync(f, 'utf8');
            let prefix = path.dirname(cwd + f).replace(cwd, '');
            let fileName = f.replace(`${prefix}/`, '');
            let minifiedFilename = fileName.replace('.js', '.min.js');
            let min = uglifyjs.minify(file);
            if (min.error) console.log(min.error);
            archive.append(min.code, {name: `${minifiedFilename}`, prefix: prefix});
            manifestFileStream = manifestFileStream.replace(new RegExp(`${prefix}/${fileName}`, "gi"), `${prefix}/${minifiedFilename}`);
        } else {
            let prefix = path.dirname(cwd + f).replace(cwd, '');
            let fileName = f.replace(`${prefix}/`, '');
            archive.file(f, {name: fileName, prefix: prefix});
        }
    });
console.log("...finished with the JS files!");

// Minfy and Append CSS Files
console.log("\nStarting the CSS files...");
let cssFiles = glob(`${cwd}/**/*.css`, {sync: true})
    .sanitize()
    .forEach(f => {
        console.log(`Archiving ${f}...`);
        if (f.indexOf('.min.') === -1) {
            let file = fs.readFileSync(f, 'utf8');
            let prefix = path.dirname(cwd + f).replace(cwd, '');
            let fileName = f.replace(`${prefix}/`, '');
            let minifiedFilename = fileName.replace('.css', '.min.css');
            let min;
            try {
                min = csso.minify(file);
                if (min.error) console.log(min.error);
            } catch (err) {
                console.error(`${f} had an issue.`, err);
                return;
            }

            archive.append(min.css, {name: `${minifiedFilename}`, prefix: prefix});
            manifestFileStream = manifestFileStream.replace(new RegExp(`${prefix}/${fileName}`, "gi"), `${prefix}/${minifiedFilename}`);
        } else {
            let prefix = path.dirname(cwd + f).replace(cwd, '');
            let fileName = f.replace(`${prefix}/`, '');
            archive.file(f, {name: fileName, prefix: prefix});
        }
    });
console.log("...finished with the CSS files!");

// Append Template HTML files
console.log("\nStarting the HTML files...");
let templateHtmlFiles = glob(`${cwd}/**/*.html`, {sync: true})
    .sanitize()
    .forEach(f => {
        console.log(`Archiving ${f}`);
        let file = fs.readFileSync(f, 'utf8');
        let prefix = path.dirname(cwd + f).replace(cwd, '');
        let fileName = f.replace(`${prefix}/`, '');
        archive.file(f, {name: fileName, prefix: prefix});
    });
console.log("...finished with the HTML files!\n");

// Switch out confidential data-points in manifest
manifestFileStream = manifestFileStream
    .replace('{{CLIENT_ID}}', process.env.CLIENT_ID)
    .replace('{{MANIFEST_KEY}}', process.env.MANIFEST_KEY)

// Append minified Manifest
archive.append(manifestFileStream, {name: 'manifest.json'});

// finalize the archive (ie we are done appending files but streams have to finish yet)
// 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
archive.finalize();

